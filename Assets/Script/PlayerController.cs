﻿using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameColor.ColorType color;
    [HideInInspector]
    public GameColor playerColor;

    [Header("Child settings")]
    public GameObject circle;
    public GameObject diamond;
    public GameObject detecter;
    public GameObject top_circle;

    public GameObject highlighter;


    [Header("Trail settings")]
    public int startTrail = 5;
    [HideInInspector]
    public int currentTrail = 0;
    public float distancePerTrail = 2f;

    public GameObject trail_prefabs;
    public Transform trails_group;

    [Header("Scale settings")]

    [Range(0f, 1f)]
    public float minScale = 0.2f;
    Vector3 originScale;
    float deltaScale = 0f;


    Vector3 lastTrailPos;

    [Header("Speed")]
    public float speed = 1f;
    public float speed_slow = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        switch (color)
        {
            case GameColor.ColorType.PINK:
                playerColor = GameController.instance.pink;
                break;
            case GameColor.ColorType.ORANGE:
                playerColor = GameController.instance.orange;
                break;
        }

        //lastTrailPos = transform.position;
        //startTrail += 1;
        currentTrail = startTrail;
        // spawnTrail();

        //originScale = circle.transform.localScale;        
        //deltaScale = (1 - minScale) / startTrail;

        originScale = Vector3.one * 1.2f;
        deltaScale = (1 - minScale) / 6;

        circle.GetComponent<SpriteRenderer>().color = playerColor.color;
        diamond.GetComponent<SpriteRenderer>().color = playerColor.color;
        top_circle.GetComponent<SpriteRenderer>().color = playerColor.color;
    }

    // Update is called once per frame
    void Update()
    {
        move();

        circle.transform.localScale = originScale + Vector3.one * (deltaScale * (currentTrail - 1));
        //circle.transform.localScale += Vector3.one * (deltaScale * (lastTrailPos - transform.position).magnitude / distancePerTrail) * 1f;

        GetComponent<CircleCollider2D>().radius = circle.transform.localScale.x / 2;

        top_circle.transform.localScale = circle.transform.localScale * 0.8f;

        //detecter.transform.localScale = circle.transform.localScale * 1.5f;



    }

    void move()
    {

        if (GameController.instance.currentPlayer != this) return;

        var speed = this.speed;

        if (currentTrail <= 0)
        {
            speed = this.speed_slow;
        }

        var transformVector = new Vector3(
            Input.GetAxis("Horizontal"),
            Input.GetAxis("Vertical"),
            0
        );

        var moveDistance = transformVector.normalized * speed * Time.deltaTime;

        //Ray r = new Ray(transform.position, transformVector.normalized);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transformVector.normalized, 999f, LayerMask.GetMask("Obstacle"));

        if (hit)
        {
            //Debug.Log(hit.collider.name);
            if (hit.distance < moveDistance.magnitude + circle.transform.localScale.x / 2)
            {
                moveDistance = moveDistance.normalized * (hit.distance - circle.transform.localScale.x / 2);
            }
        }

        transform.position += moveDistance;

        if ((lastTrailPos - transform.position).magnitude > distancePerTrail)
        {
            spawnTrail();
        }

    }

    public void highlight()
    {
        StartCoroutine(highlightTransition());
    }

    IEnumerator highlightTransition()
    {

        highlighter.GetComponent<SpriteRenderer>().color = playerColor.getOppositeColor().backgroundColor;

        var duration = 0.5f;
        var time = 0f;
        var originColor = highlighter.GetComponent<SpriteRenderer>().color;
        while (time < duration)
        {
            time += Time.deltaTime;
            var lerpValue = Utility.easeOutCubic(time / duration);

            highlighter.transform.localScale = Vector3.Lerp(
                Vector3.one,
                Vector3.one * 5f,
                lerpValue
            );

            var targetColor = new Color(
                originColor.r,
                originColor.g,
                originColor.b,
                0
            );

            highlighter.GetComponent<SpriteRenderer>().color = Color.Lerp(
                originColor,
                targetColor,
                time / duration
            );


            yield return null;
        }
    }

    void spawnTrail()
    {
        if (isDashing) return;
        if (currentTrail <= 0) return;

        foreach (Transform t in trails_group)
        {
            if (detecter.GetComponent<CircleCollider2D>().ClosestPoint(t.position) == (Vector2)t.position)
            {
                return;
            }
        }

        lastTrailPos = transform.position;
        var trail = Instantiate(trail_prefabs, transform.position, Quaternion.identity, trails_group);

        switch (playerColor.type)
        {
            case GameColor.ColorType.PINK:
                trail.layer = 8;
                break;
            case GameColor.ColorType.ORANGE:
                trail.layer = 9;
                break;
        }

        trail.GetComponent<TrailController>().type = playerColor.type;
        trail.GetComponent<TrailController>().setSubTrialColor(playerColor.color); // subtrail
    }
    void pickTrail(GameObject trail)
    {
        StartCoroutine(pickTrail_coroutine(trail));
    }

    [HideInInspector]
    public bool isDashing = false;

    public void dash()
    {
        StartCoroutine(dashSequence());
    }

    IEnumerator dashSequence()
    {

        isDashing = true;
        var selectedTrails = GameController.instance.selectedTrails;
        // Debug.Log(selectedTrails.Count);-

        var duration = 0.25f;

        for (int i = 0; i < selectedTrails.Count; i++)
        {
            var trail = selectedTrails[i];
            var time = 0f;
            while (time < duration)
            {

                var lerpVal = Utility.easeOutSine(time / duration);

                time += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, trail.transform.position, lerpVal);
                yield return null;
            }
            //GameController.instance.updateLineRenderer();
            quick_pickTrail(trail.gameObject);
            yield return new WaitForEndOfFrame();
            GameController.instance.removeVertexLineRenderer();
            duration *= 0.7f;
        }
        selectedTrails.Clear();
        // GameController.instance.updateLineRenderer();
        isDashing = false;
        yield return new WaitForEndOfFrame();
        GameController.instance.setPickable(true);
    }

    IEnumerator pickTrail_coroutine(GameObject trail)
    {

        bool increaseTrail = false;

        var duration = 0.25f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            var tweenVal = Utility.easeOutSine(time / duration);
            trail.transform.position = Vector3.Lerp(trail.transform.position, transform.position, tweenVal.Remap(0, 1, 0, 0.15f));

            if (!increaseTrail && (time / duration) >= 0.75f)
            {

                currentTrail += 1;
                increaseTrail = true;
            }

            yield return null;
        }
        Destroy(trail);
        lastTrailPos = transform.position;
    }

    void quick_pickTrail(GameObject trail)
    {
        lastTrailPos = transform.position;
        currentTrail += 1;
        Destroy(trail);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Trails") && other.GetComponent<TrailController>().pickAble)
        {
            //Debug.Log("?");
            //Debug.Log(other.GetComponent<TrailController>().pickAble);
            // other.GetComponent<TrailController>().snapToPlayer(this);
            pickTrail(other.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Trails") && !isDashing && !other.GetComponent<TrailController>().pickAble)
        {
            other.GetComponent<TrailController>().pickAble = true;
            currentTrail -= 1;
            if (currentTrail <= 0)
            {
                currentTrail = 0;
                highlight();
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class GameColor : ScriptableObject
{

    public enum ColorType
    {
        PINK,
        ORANGE
    }

    public ColorType type;
    public Color color;
    public Color backgroundColor;

    public GameColor getOppositeColor()
    {
        switch (type)
        {
            case ColorType.PINK:
                return GameController.instance.orange;
            case ColorType.ORANGE:
                return GameController.instance.pink;
            default:
                return null;
        }
    }


}

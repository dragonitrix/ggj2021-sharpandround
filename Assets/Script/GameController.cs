﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController instance;

    public System.Action onPlayerChange;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    [Header("Win Condition settings")]
    public bool isLevelComplete = false;
    public List<WinConditionCheck> winConditionChecks = new List<WinConditionCheck>();

    [Header("Level settings")]
    public bool isSecondPlayerNeed = true;


    [Header("Camera")]
    public Camera mainCamera;
    public Cinemachine.CinemachineVirtualCamera virtualCamera;

    [Header("Color Settings")]
    public GameColor pink;
    public GameColor orange;

    [Header("Prefabs")]
    public GameObject bg_prefab;

    [Header("UI")]
    public RectTransform completeTransition;

    public bool isSelecting = false;

    bool isDashing;
    public Transform pink_trails_group;
    public Transform orange_trails_group;

    public LineRenderer lineRenderer;

    public List<TrailController> selectedTrails = new List<TrailController>();

    public PlayerController pink_player;
    public PlayerController orange_player;

    [HideInInspector]
    public PlayerController currentPlayer;

    // Start is called before the first frame update
    void Start()
    {

        setCurrentPlayer(pink_player);
    }

    public void setCurrentPlayer(PlayerController targetPlayer)
    {
        currentPlayer = targetPlayer;
        virtualCamera.Follow = currentPlayer.transform;
    }

    public void changePlayer()
    {
        if (!isSecondPlayerNeed) return;

        setCurrentPlayer(getOppositePlayer(currentPlayer));

        //transition here

        onPlayerChange();


        lineRenderer.startColor = currentPlayer.playerColor.getOppositeColor().backgroundColor;
        lineRenderer.endColor = currentPlayer.playerColor.getOppositeColor().backgroundColor;

        // currentPlayer.highlight(); //suck

        StartCoroutine(changePlayerTransition());
    }

    IEnumerator changePlayerTransition()
    {
        var bg = Instantiate(bg_prefab, currentPlayer.transform.position + Vector3.forward * 1f, Quaternion.identity);

        bg.GetComponent<SpriteRenderer>().color = currentPlayer.playerColor.getOppositeColor().backgroundColor;
        yield return new WaitForEndOfFrame();
        mainCamera.backgroundColor = currentPlayer.playerColor.backgroundColor;

        var duration = 0.5f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            // lerpValue = (time / duration);
            var lerpValue = Utility.easeOutCubic(time / duration);

            bg.transform.localScale = Vector3.Lerp(
                Vector3.one * 50f,
                Vector3.zero,
                lerpValue
            );

            yield return null;
        }
        Destroy(bg.gameObject);
    }

    public PlayerController getOppositePlayer()
    {
        return getOppositePlayer(currentPlayer);
    }

    public PlayerController getOppositePlayer(PlayerController currentPlayer)
    {
        switch (currentPlayer.playerColor.type)
        {
            case GameColor.ColorType.PINK:
                return orange_player;
            case GameColor.ColorType.ORANGE:
                return pink_player;
            default:
                return null;
        }
    }

    public void dash()
    {
        setPickable(false);

        // foreach (TrailController t in selectedTrails)
        // {
        //     t.pickAble = true;
        // }

        currentPlayer.dash();
    }

    public void setPickable(bool value)
    {
        foreach (Transform t in pink_trails_group)
        {
            t.GetComponent<TrailController>().pickAble = value;
        }
        foreach (Transform t in orange_trails_group)
        {
            t.GetComponent<TrailController>().pickAble = value;
        }
    }

    public void selectTrail(TrailController trail)
    {
        selectedTrails.Add(trail);
        updateLineRenderer();


    }

    public void updateLineRenderer()
    {

        lineRenderer.positionCount = (selectedTrails.Count);

        for (int i = 0; i < selectedTrails.Count; i++)
        {
            lineRenderer.SetPosition(i, selectedTrails[i].transform.position);
        }
    }

    public void removeVertexLineRenderer()
    {
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            if (i < lineRenderer.positionCount - 1)
            {
                lineRenderer.SetPosition(i, lineRenderer.GetPosition(i + 1));
            }
        }
        lineRenderer.positionCount -= 1;
    }

    float completeDelay = 0f;

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            changePlayer();
        }


        if (Input.GetMouseButtonUp(0) && isSelecting)
        {
            isSelecting = false;
            //Debug.Log("dash");
            dash();
        }

        if (!isLevelComplete)
        {
            var completeResult = true;
            foreach (var item in winConditionChecks)
            {
                if (!item.isFullfill)
                {
                    completeResult = false;
                }
            }

            if (completeResult)
            {
                completeDelay += Time.deltaTime;
            }
            if (completeDelay >= 1f)
            {
                isLevelComplete = true;
                levelComplete();
            }
        }

    }



    public void levelComplete()
    {
        Debug.Log("win!!!");
        StartCoroutine(levelCompleteTransition());
    }

    IEnumerator levelCompleteTransition()
    {
        yield return new WaitForSeconds(1f);
        var duration = 1f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            completeTransition.sizeDelta = Vector2.Lerp(
                Vector2.zero,
                Vector2.one * 700,
                lerpValue
            );

            yield return null;
        }

        yield return new WaitForSeconds(1f);

        LevelLoader.instance.nextScene();

    }


}

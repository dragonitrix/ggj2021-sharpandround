﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager_Game : MonoBehaviour
{

    public Button restart_button;
    public Button home_button;

    // Start is called before the first frame update
    void Start()
    {
        restart_button.onClick.AddListener(LevelLoader.instance.restartScene);
        home_button.onClick.AddListener(LevelLoader.instance.backToMenu);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

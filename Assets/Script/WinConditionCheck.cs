﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinConditionCheck : MonoBehaviour
{

    [Header("Is fullfill")]
    public bool isFullfill = false;

    [Header("settings")]
    public bool isNeedPlayer = false;
    public GameColor.ColorType color;

    public int pink_trailNeed = 0;
    public int current_pink_trailNeed = 0;
    public bool isPinkPlayerInside = false;
    public int orange_trailNeed = 0;
    public int current_orange_trailNeed = 0;
    public bool isOrangePlayerInside = false;

    [Header("Prefabs")]
    public GameObject miniTrail;
    public Transform miniTrailGroup;
    List<GameObject> miniTrailList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        var trailWidth = 1f;
        var gap = 0.5f;

        var trailneed = pink_trailNeed + orange_trailNeed;

        var totalWidth = trailWidth * trailneed + gap * (trailneed - 1);

        for (int i = 0; i < trailneed; i++)
        {
            var spawnPos = new Vector3(
                transform.position.x - totalWidth / 2 + trailWidth / 2 + trailWidth * i + gap * i,
                transform.position.y,
                transform.position.z
            );

            var trail = Instantiate(miniTrail, spawnPos, Quaternion.identity, miniTrailGroup);

            if (i < pink_trailNeed)
            {
                trail.GetComponent<SpriteRenderer>().color = GameController.instance.pink.color;
            }
            else
            {
                trail.GetComponent<SpriteRenderer>().color = GameController.instance.orange.color;
            }

            miniTrailList.Add(trail);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("pink: " + current_pink_trailNeed);
        // Debug.Log("oragne: " + current_orange_trailNeed);

        //current_pink_trailNeed = 0;
        //current_orange_trailNeed = 0;

        var tempPink = current_pink_trailNeed;
        //if (isPinkPlayerInside)
        //{
        //    tempPink += GameController.instance.pink_player.currentTrail;
        //}
        var tempOrange = current_orange_trailNeed;
        //if (isOrangePlayerInside)
        //{
        //    tempOrange += GameController.instance.orange_player.currentTrail;
        //}


        if (tempPink >= pink_trailNeed && tempOrange >= orange_trailNeed)
        {
            isFullfill = true;
        }
        else
        {
            isFullfill = false;
        }

        var greenthings = transform.GetChild(1).GetChild(0);
        if (isFullfill)
        {
            greenthings.localScale = Vector3.Lerp(greenthings.localScale, Vector3.one * 1.1f, Time.deltaTime * 5f);
        }
        else
        {
            greenthings.localScale = Vector3.Lerp(greenthings.localScale, Vector3.one, Time.deltaTime * 5f);
        }

        for (int i = 0; i < miniTrailList.Count; i++)
        {
            miniTrailList[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            if (i < pink_trailNeed)
            {
                if (tempPink > 0)
                {
                    miniTrailList[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                    tempPink--;
                }
            }
            else
            {
                if (tempOrange > 0)
                {
                    miniTrailList[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                    tempOrange--;
                }

            }
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Trails"))
        {
            switch (other.GetComponent<TrailController>().type)
            {
                case GameColor.ColorType.PINK:
                    current_pink_trailNeed++;
                    break;
                case GameColor.ColorType.ORANGE:
                    current_orange_trailNeed++;
                    break;
            }
        }
        if (other.CompareTag("Player"))
        {
            switch (other.GetComponent<PlayerController>().playerColor.type)
            {
                case GameColor.ColorType.PINK:
                    isPinkPlayerInside = true;
                    break;
                case GameColor.ColorType.ORANGE:
                    isOrangePlayerInside = true;
                    break;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Trails"))
        {
            switch (other.GetComponent<TrailController>().type)
            {
                case GameColor.ColorType.PINK:
                    current_pink_trailNeed--;
                    break;
                case GameColor.ColorType.ORANGE:
                    current_orange_trailNeed--;
                    break;
            }
        }
        if (other.CompareTag("Player"))
        {
            switch (other.GetComponent<PlayerController>().playerColor.type)
            {
                case GameColor.ColorType.PINK:
                    isPinkPlayerInside = false;
                    break;
                case GameColor.ColorType.ORANGE:
                    isOrangePlayerInside = false;
                    break;
            }
        }
    }

}

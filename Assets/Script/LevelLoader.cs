﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Image transition;

    public static LevelLoader instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    private void Start()
    {

        StartCoroutine(fadeinCoroutine());
    }
    private void Update()
    {
    }

    public void jumpToScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void jumpToScene(int index)
    {
        //SceneManager.LoadScene(index);
        StartCoroutine(triggerAnim(index));
    }

    public void backToMenu()
    {
        jumpToScene(0);
        //SceneManager.LoadScene(0);
    }

    public void nextScene()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        jumpToScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void restartScene()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        jumpToScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator fadeinCoroutine()
    {
        var duration = 1f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            transition.transform.localScale = Vector3.Lerp(
                Vector3.one * 10f,
                Vector3.zero,
                lerpValue
            );

            yield return null;
        }
    }

    IEnumerator triggerAnim(int scene)
    {

        var duration = 1f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            transition.transform.localScale = Vector3.Lerp(
                Vector3.zero,
                Vector3.one * 10f,
                lerpValue
            );

            yield return null;
        }
        yield return SceneManager.LoadSceneAsync(scene);


        duration = 1f;
        time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            transition.transform.localScale = Vector3.Lerp(
                Vector3.one * 10f,
                Vector3.zero,
                lerpValue
            );

            yield return null;
        }

    }
}

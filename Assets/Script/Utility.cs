﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    public static float easeOutCubic(float x)
    {
        return 1 - Mathf.Pow(1 - x, 3);
    }
    public static float easeOutSine(float x)
    {
        return Mathf.Sin((x * Mathf.PI) / 2);
    }
}

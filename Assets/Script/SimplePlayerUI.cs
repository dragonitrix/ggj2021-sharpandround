﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimplePlayerUI : MonoBehaviour
{

    public PlayerController playerController;
    public Transform UITrailGroup;


    // Start is called before the first frame update
    void Start()
    {

        // for (int i = 0; i < UITrailGroup.transform.childCount; i++)
        // {
        //     if (i < playerController.currentTrail)
        //     {
        //         UITrailGroup.transform.GetChild(0).GetComponent<Image>().enabled = true;
        //     }
        //     else
        //     {
        //         UITrailGroup.transform.GetChild(0).GetComponent<Image>().enabled = false;
        //     }
        // }

    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < UITrailGroup.transform.childCount; i++)
        {
            if (i < playerController.currentTrail)
            {
                UITrailGroup.transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                UITrailGroup.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

    }
}

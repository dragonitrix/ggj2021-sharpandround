﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameController.instance.onPlayerChange += onPlayerChange;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void onPlayerChange()
    {
        StartCoroutine(onPlayerChangeTransition());
    }

    IEnumerator onPlayerChangeTransition()
    {
        var duration = 0.5f;

        var originColor = GameController.instance.currentPlayer.playerColor.backgroundColor;
        var targetColor = GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor;
        var child = transform.GetChild(0).GetComponent<SpriteRenderer>();
        child.enabled = true;
        child.color = originColor;
        GetComponent<SpriteRenderer>().color = targetColor;

        yield return new WaitForSeconds(duration);

        child.enabled = false;


        /*
        var originColor = GameController.instance.currentPlayer.playerColor.backgroundColor;
        var targetColor = GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor;

        var duration = 0.5f;
        var time = 0f;
        while (time < duration)
        {
            var lerpVal = Utility.easeOutSine(time / duration);
            time += Time.deltaTime;

            GetComponent<SpriteRenderer>().color = Color.Lerp(originColor, targetColor, lerpVal);

            yield return null;
        }
        */


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SimpleTutorial1 : MonoBehaviour
{

    public TextMeshPro tm1;
    public TextMeshPro tm2;
    public TextMeshPro tm3;

    bool trigger1 = false;
    bool trigger2 = false;
    bool trigger3 = false;


    float moveTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(fadeIn(tm1));
    }

    // Update is called once per frame
    void Update()
    {
        var transformVector = new Vector3(
            Input.GetAxis("Horizontal"),
            Input.GetAxis("Vertical"),
            0
        );
        if (!trigger1 && transformVector.magnitude != 0)
        {
            moveTime += Time.deltaTime;
            if (moveTime >= 1.5f)
            {
                trigger1 = true;
                StartCoroutine(fadeOut(tm1, 1f));
            }
        }

        if (!trigger2 && GameController.instance.currentPlayer.currentTrail <= 0)
        {
            trigger2 = true;
            StartCoroutine(fadeIn(tm2));
        }

        if (!trigger3 && GameController.instance.currentPlayer.isDashing)
        {
            trigger3 = true;
            StartCoroutine(fadeOut(tm2));
            StartCoroutine(fadeIn(tm3));
        }


    }

    IEnumerator fadeIn(TextMeshPro tm, float delay = 0f)
    {
        yield return new WaitForSeconds(delay);
        var duration = 3f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            var originColor = new Color(
                tm.color.r,
                tm.color.g,
                tm.color.b,
                0
            );

            var targetColor = new Color(
                tm.color.r,
                tm.color.g,
                tm.color.b,
                1
            );

            tm.color = Color.Lerp(
                originColor,
                targetColor,
                lerpValue
            );
            yield return null;
        }
    }

    IEnumerator fadeOut(TextMeshPro tm, float delay = 0f)
    {
        yield return new WaitForSeconds(delay);
        var duration = 3f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var lerpValue = Utility.easeOutCubic(time / duration);

            var originColor = new Color(
                tm.color.r,
                tm.color.g,
                tm.color.b,
                1
            );

            var targetColor = new Color(
                tm.color.r,
                tm.color.g,
                tm.color.b,
                0
            );

            tm.color = Color.Lerp(
                originColor,
                targetColor,
                lerpValue
            );
            yield return null;
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    public GameColor.ColorType type;
    public bool pickAble = false;
    public GameObject subtrail;
    public GameObject highlighter;

    Color originColor;
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            var lerpVal = 0f;
            lerpVal = (other.transform.position - transform.position).magnitude / 3f;
            //            Debug.Log(lerpVal);
            subtrail.GetComponent<SpriteRenderer>().color = Color.Lerp(other.GetComponent<PlayerController>().playerColor.color, originColor, lerpVal);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            subtrail.GetComponent<SpriteRenderer>().color = originColor;
        }
    }
    public void setSubTrialColor(Color color)
    {
        originColor = color;
        subtrail.GetComponent<SpriteRenderer>().color = originColor;
    }

    private void Update()
    {
        if (isSelected)
        {
            subtrail.GetComponent<SpriteRenderer>().color = GameController.instance.getOppositePlayer().playerColor.backgroundColor;
        }
    }

    bool isSelected;

    private void OnMouseDown()
    {
        if (!pickAble) return;
        GameController.instance.isSelecting = true;
        GameController.instance.selectTrail(this);
        isSelected = true;
    }

    private void OnMouseEnter()
    {
        if (!pickAble) return;
        if (GameController.instance.isSelecting && !isSelected)
        {
            GameController.instance.selectTrail(this);
            isSelected = true;
        }
    }

    private void OnMouseUp()
    {
        //        GameController.instance.dash();
    }

    private void OnMouseOver()
    {

        var time = Time.deltaTime * 10f;

        var a = Mathf.Lerp(highlighter.GetComponent<SpriteRenderer>().color.a, 0.25f, time);

        highlighter.GetComponent<SpriteRenderer>().color = new Color(
            GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.r,
            GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.g,
            GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.b,
            a
        );

        highlighter.transform.localScale = Vector3.Lerp(
            highlighter.transform.localScale,
            2f * Vector3.one,
            time
        );

    }

    private void OnMouseExit()
    {
        StartCoroutine(fadeOut_Coroutine());
    }

    IEnumerator fadeOut_Coroutine()
    {

        var duration = 0.25f;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            var a = (time / duration).Remap(0, 1, 0.25f, 0);

            highlighter.GetComponent<SpriteRenderer>().color = new Color(
                GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.r,
                GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.g,
                GameController.instance.currentPlayer.playerColor.getOppositeColor().backgroundColor.b,
                a
            );


            highlighter.transform.localScale = Vector3.Lerp(
                highlighter.transform.localScale,
                Vector3.one,
                time / duration
            );

            yield return null;
        }

    }
}

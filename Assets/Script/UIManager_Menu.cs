﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager_Menu : MonoBehaviour
{

    public Button start_button;
    public Button credit_button;

    // Start is called before the first frame update
    void Start()
    {
        start_button.onClick.AddListener(LevelLoader.instance.nextScene);
        credit_button.onClick.AddListener(LevelLoader.instance.backToMenu);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
